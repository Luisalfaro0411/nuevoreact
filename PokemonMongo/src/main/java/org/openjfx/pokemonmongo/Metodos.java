/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.pokemonmongo;

import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import java.util.Arrays;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

/**
 *
 * @author Manana_pos5
 */
public class Metodos {
    
    public void eliminar(MongoDatabase db , String nombre, String key , String value){
            MongoCollection<Document> collecion = db.getCollection(nombre);
            Document findDocument = new Document(key,value);
            collecion.deleteMany(findDocument);
            System.out.println("DOCUMENTOS ELIMINADOS");
        }
    
    public void insertar(MongoDatabase db , String nombre , String campo , String valor){
     
            MongoCollection<Document> collection = db.getCollection(nombre);
            try {
                InsertOneResult result = collection.insertOne(new Document()
                        .append("_id", new ObjectId())
                        .append(campo , valor));
                        
                System.out.println("DOCUMENTO INSERTADO: " + result.getInsertedId());
            } catch (MongoException me) {
                System.err.println("Unable to insert due to an error: " + me);
            }
        
    }
    
    public MongoCollection<Document> listarCollection(MongoDatabase db,String nombre){
            System.out.println("SE LISTARA TODOS LOS CAMPOS DEL POKEMON");
            MongoCollection<Document> collection= db.getCollection(nombre);
            Document doc= collection.find().first();
            System.out.println(doc.toJson());
            return collection;
        }
    
     public void buscar(MongoDatabase db , String nombre , String key , String value){
            MongoCollection<Document> collection = db.getCollection(nombre);
            Document findDocument = new Document(key , value);
            

            MongoCursor<Document> resultDocument = (MongoCursor<Document>) collection.find(findDocument).iterator();

            System.out.println("LISTANDO POR: " +  key.toUpperCase());
            while(resultDocument.hasNext()){
               
                System.out.println("NOMBRE DEL POKEMON: " + resultDocument.next().get(key));
                
            }
        }
     
     public void update(MongoDatabase db , String nombre , String campo , String valor){
        
            MongoCollection<Document> collection = db.getCollection(nombre);
            Document query = new Document().append(campo , valor);
            Bson updates = Updates.combine(
                    Updates.set("type2", "FUEGO"),
                    Updates.currentTimestamp("lastUpdated"));
            UpdateOptions options = new UpdateOptions().upsert(true);
            try {
                UpdateResult result = collection.updateOne(query, updates, options);
                System.out.println("Modified document count: " + result.getModifiedCount());
                System.out.println("Upserted id: " + result.getUpsertedId()); // only contains a value when an upsert is performed
            } catch (MongoException me) {
                System.err.println("Unable to update due to an error: " + me);
            }
        
     }
    
    
}

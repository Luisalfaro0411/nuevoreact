/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.pokemonmongo;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

/**
 *
 * @author Manana_pos5
 */
public class Conexion {
    private String url = "mongodb+srv://luisalfaroDAM2:itep@cluster0.dpuaajd.mongodb.net/?retryWrites=true&w=majority";
    private String database="sample_airbnb";
    private MongoClient client;
    
    public MongoDatabase connect(String url,String nombreDB){
        client = MongoClients.create(url);
        MongoDatabase database = client.getDatabase(nombreDB);
        System.out.println("CONEXION ESTABLECIDA");
        return database;
    }
    
    public void disconnect(){
        client.close();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }
}
